package com.example.calendario_exemplo;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;

public class GridViewAdapter extends BaseAdapter {

    private static int NUM_ROWS = 6;
    private static float HEADER_HEIGHT = 50f;
    private static int STATUS_BAR_APPROXIMATE_HEIGHT = 16;

    private Context context;
    private ArrayList<Celula> celulasList;

    public GridViewAdapter(Context context, ArrayList<Celula> celulasList) {
        this.context = context;
        this.celulasList = celulasList;
    }

    @Override
    public int getCount() {
        return celulasList.size();
    }

    @Override
    public Object getItem(int position) {
        return celulasList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewholder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_celula, null);

            convertView.setLayoutParams(new GridView.LayoutParams(GridView.LayoutParams.MATCH_PARENT, getElementHeight()));

            viewholder = new ViewHolder();

            viewholder.textView1 = (TextView) convertView.findViewById(R.id.textView1);

            convertView.setTag(viewholder);

        } else {

            viewholder = (ViewHolder) convertView.getTag();
        }

        Celula celula = celulasList.get(position);

        viewholder.textView1.setText(String.valueOf(celula.getCelula_id()));

        return convertView;
    }

    private static class ViewHolder {
        TextView textView1;
    }

    private int getElementHeight() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int screenHeight = displayMetrics.heightPixels;

        //remove the header height
        screenHeight -= TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, HEADER_HEIGHT,
                context.getResources().getDisplayMetrics());

        return (screenHeight / NUM_ROWS) - STATUS_BAR_APPROXIMATE_HEIGHT;
    }
}